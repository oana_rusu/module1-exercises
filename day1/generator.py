import codecs


def non_empty_lines(file):
    for line in file:
        if line.strip():
            yield line

def numbered_lines(lines):
    # yields lines
    for index, line in enumerate(lines):
       yield str(index) + ': ' + line

def wrap_at_column(lines, w):
    # yields lines
    for line in lines:
        yield line[:w]
        if len(line) > w:
            yield line[w:]

def paginate(lines, page_size, page_separator='\n\n'):
    # yields lines
    page_size_count = 0

    for line in lines:
        page_size_count += 1
        yield line

        if page_size_count == page_size:
            page_size_count = 0
            yield page_separator


def print_lines(lines):
    for l in lines:
        print l,

# you can combine these in arbitrary ways

file = 'webpage.txt'
lines = codecs.open(file, encoding='utf-8')

# print_lines(numbered_lines(lines))
#
print_lines(
    paginate(
        wrap_at_column(
            numbered_lines(
               non_empty_lines(lines),
            ),
            5
        )
       ,
        5,
        "\n---------\n"
    )
)

# print_lines(lines)

# lines = numbered_lines(lines)
# lines = wrap_at_column(lines, 10)
# lines = paginate(lines, 10)
# print_lines(lines)