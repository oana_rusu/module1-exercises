import json


def decorator_json(func):
    def wrapped(*args, **kwargs):
        return json.dumps(func(*args, **kwargs))
    return wrapped

@decorator_json
def build_json(data):
    return data

data = {1: 'a', 2: 'b', 3: 'c'}
# print build_json(data)


def validate_unicode(func):
    def wrapped(*args, **kwargs):
        for value in list(args) + kwargs.values():
            if type(value) != unicode:
                raise TypeError('expected unicode %s ' % value)

        result = func(*args, **kwargs)
# if not isinstance(arg, unicode)
        if type(result) != unicode:
            raise TypeError('expected unicode')

        return result
    return wrapped

@validate_unicode
def return_data(*args, **kwargs):
    data = ''

    for value in list(args) + kwargs.values():
        data += value
    return data

print return_data(u'aaaa', u'bbbbbb')
