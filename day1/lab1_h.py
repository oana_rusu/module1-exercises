# Homework problems for unit 1
# ----------------------------

#The rest of dividing a square number like 81, by 32 can only be
#0, 1, 4, 9, 16, 17, 25
#A number that has such a rest when divided by 32 is called a pseudo square mod 32
def pseudo_square_mod32(n):
    return True if n % 32 in [0, 1, 4, 9, 16, 17, 25] else False

def certain_square(n):
    return (n**0.5 + 0.5)**2 == n

# Test how useful is the maybe_square test.
# How many false positives it reports for numbers under 1000?
# What percentage of tests are false positives?
def maybe_square_accuracy():
    N = 10000
    false_pos = 0

    for x in xrange(N):
        if pseudo_square_mod32(x) and not certain_square(x):
            false_pos += 1

    return "False positives: " + str(false_pos) + "\n False positives percentage: " + str(false_pos / N * 100)


# Given a list of numbers, return a list where
# all adjacent == elements have been reduced to a single element,
# so [1, 2, 2, 3] returns [1, 2, 3]. You may create a new list or
# modify the passed in list.
def remove_adjacent(nums):
    pass

# rescale transforms the range fm..fM to tm..tM
# Example: if f iterates from 0 to 10 and tm, tM == 0, 1
# then rescale(f) should be 0.0, 0.1, 0.2 ... 1.0
def rescale(f, fm, fM, tm, tM):
    f = float(f)
    # todo
    return f

def test_pseudo_square_mod32():
    assert pseudo_square_mod32(9)
    assert not pseudo_square_mod32(24)
    assert pseudo_square_mod32(17)

def test_remove_adjacent():
    assert remove_adjacent([1, 2, 2, 3]) == [1, 2, 3]
    assert remove_adjacent([2, 2, 3, 3, 3]) == [2, 3]
    assert remove_adjacent([]) == []

def test_rescale():
    assert rescale(5, 0, 10, 0, 1) == 0.5

def test_maybe_square_accuracy():
    assert maybe_square_accuracy() == "False positives: 4\n False positives percentage: 4"


