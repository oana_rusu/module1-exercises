import os

def cond_1(d):
    return len(d) < 4
def cond_2(d):
    return '.' not in d
def cond_3(d):
    return d.startswith('.')

def action_1(d):
    print 'a1', d

def action_2(d):
    print 'a2', d

def action_3(d):
    print 'a3', d

# decupleaza dispatch
_registry = {
    cond_1: action_1,
    cond_2: action_2,
    cond_3: action_3
}

def walk(d):
    for pth, dirs, files in os.walk(d):
        for d in dirs:
            for cond, act in _registry.iteritems():
                if cond(d):
                    act(d)

walk('/home')