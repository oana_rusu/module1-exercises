# coding=utf-8
import urllib2
import codecs

# Read the wikipedia page at  http://ko.wikipedia.org/wiki/위키백과:대문
# Prepend the line number to each line of the page, then save that to disk. Use utf-16 for disk storage.

# Open the saved file and print the first 3 lines.

url = u'http://ko.wikipedia.org/wiki/위키백과:대문'

url_data = urllib2.urlopen(url.encode('utf-8'))

f = codecs.open('webpage.txt', 'w', encoding='utf-8')
# counter = 0

# for line in url_data: # this will iterate line by line
#     counter += 1
#     f.write(str(counter) + ': ' + line.decode('utf-8'))

for index, line in enumerate(url_data): # this will iterate line by line
    f.write(str(index) + ': ' + line.decode('utf-8'))

f.close()

f = codecs.open('webpage.txt', encoding='utf-8')

for index, line in enumerate(f):
    print line
    if index == 2:
        break