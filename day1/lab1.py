# Exercises for notions learned in unit 1
# ---------------------------------------

# takes a message, capitalizes the first letter then underlines the message with -
# print make_title('ana')
# Ana
# ---
# use the string method upper
def make_title(message):
    return  message[0].upper() + message[1:] + "\n" + "-" * len(message)


# given a list of tuples of length 2 it returns a list of the tuples inverted
# if both values in the tuple are the same then we omit them from the output
# [(2, 4), (3, 3), (1, 2)] will become [(4, 2), (2, 1)]
def invert_tuples(lst):
    final_lst = []

    for a, b in lst:
        if a != b:
            final_lst.append((b, a))

    return final_lst

# curses is a dictionary of cursewords to censored curses
# Given a list of words replace the curses according to the dict
# censor(['you', 'fucker'], {'fucker': 'f**er'}) should return
# ['you', 'f**er']
def censor(lst, curses):
    final_lst = []

    for x in lst:
        if x in curses:
            final_lst.append(curses[x])
        else:
            final_lst.append(x)

    return final_lst

# Given a string s, return a string made of the first 2
# and the last 2 chars of the original string,
# so 'spring' yields 'spng'. However, if the string length
# is less than 2, return instead the empty string.
def both_ends(s):
   if len(s) < 2:
       return ''
   return s[:2] + s[-2:]

# return the factorial of n
# factorial(5) = 1*2*3*4*5
def factorial(n):
    if n in [0,1]:
        return 1
    else:
        return n * factorial(n-1)


# return a list of fibonacci numbers smaller than n
def fibo(n):
    fibo_result = [0, 1]
    sum  = 1

    while sum < n:
        fibo_result.append(sum)
        sum = fibo_result[-1] + fibo_result[-2]

    return fibo_result


#----- testing code below ---

def test_make_title():
    assert make_title('ana') == 'Ana\n---'

def test_invert_tuples():
    assert invert_tuples([(2, 4), (3, 3), (1, 2)]) == [(4, 2), (2, 1)]

def test_censor():
    assert censor(['you', 'fucker'], {'fucker': 'f**er'}) == ['you', 'f**er']

def test_both_ends():
    assert both_ends('spring') == 'spng'
    assert both_ends('Hello') == 'Helo'
    assert both_ends('a') == ''
    assert both_ends('xyz') == 'xyyz'

def test_fibo():
    assert fibo(10) == [0, 1, 1, 2, 3, 5, 8]

def test_factorial():
    assert factorial(5) == 1*2*3*4*5
    assert factorial(1) == 1
    assert factorial(0) == 1

