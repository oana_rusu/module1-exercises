#!/usr/bin/python

import sys
import re

def _parse_line(line):
    """
    >>> _parse_line('ana are')
    (['ana', 'are'], {})
    >>> _parse_line('ana --are 4')
    (['ana'], {'are': '4'})
    >>> _parse_line('--are 4 ana')
    Traceback (most recent call last):
    ...
    ValueError: positional arg after keyword arg
    >>> _parse_line('--34 3')
    Traceback (most recent call last):
    ...
    ValueError: keyword arg must start with letter
    >>> _parse_line('ana --are')
    Traceback (most recent call last):
    ...
    ValueError: missing keyword arg value
    """

    line = line.split(' ')

    pos_args = []
    key_args = {}

    is_keyword_arg = 0
    stop_pos_arg = 0

    for index in xrange(len(line)):
        if is_keyword_arg == 1 and line[index].startswith('--'):
            raise ValueError('missing keyword arg value')
        elif is_keyword_arg == 1:
            key_args[line[index-1][2:]] = line[index]
            is_keyword_arg = 0
        elif not line[index].startswith('--') and not stop_pos_arg:
            pos_args.append(line[index])
        elif not line[index].startswith('--') and stop_pos_arg:
            raise ValueError('positional arg after keyword arg')
        elif line[index][2:].isdigit():
            raise ValueError('keyword arg must start with letter')
        else:
            stop_pos_arg = 1
            is_keyword_arg = 1

    if is_keyword_arg == 1:
        raise ValueError('missing keyword arg value')

    # print 'Argument List:', str(pos_args), str(key_args)

    return (pos_args, key_args)

def command(func):
    _registry[func.__name__] = func
    return func

def function1(*args, **kwargs):
    print (args, kwargs), 'from ana'

def function2(*args, **kwargs):
    print (args, kwargs), 'from test'

_registry = {
    'function1': function1,
    'function2': function2,
}


def main():
    """
    usage:
    script.py function_name arg1 arg2 ... --kwarg1 value1 --kwarg2 value2 ...
    """
    args, kwargs = _parse_line(' '.join(sys.argv[1:]))

    if not args:
        sys.stderr.write('no command given')

    if len(args) > 1 and _registry[args[0]]:
        registry_function = args[0]

        if registry_function not in _registry:
            sys.stderr.write('unknown command %s' % registry_function)

        args = args[1:]

        _registry[registry_function](*args, **kwargs)


