from lcom import command, main
from image_storage import copy_to_directory
from test_db import load_from_json

def internal():
    pass

@command
def ls(pth):
    print pth, 'from pth'


@command
def copy(f, to):
    print f, to, 'from cpy'

@command
def add_gallery(gallery, image_path):
    new_image_path = copy_to_directory(image_path)

    gallerydb = load_from_json('db.json')
    gallerydb.add_to_catalog('galleries', gallery, new_image_path)


@command
def show_gallery(gallery, format):


main()
