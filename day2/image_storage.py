import os
import uuid
import shutil
import urllib2

def copy_to_directory(pth):

    if not os.path.exists(pth) or not os.path.isfile(pth):
        open(pth, 'w')

    new_name = str(uuid.uuid4())

    if not os.path.exists('image_storage'):
        os.mkdir('image_storage')

    if not os.path.isdir(os.path.join('image_storage', new_name[0])):
        os.mkdir(os.path.join('image_storage', new_name[0]))

    shutil.copy(pth, os.path.join('image_storage', new_name[0], new_name))

    return os.path.join('image_storage', new_name[0], new_name)


# path = 'image_test.jpg'
# copy_to_directory(path)

