import json
import os

class SimpleDb(object):
    def __init__(self, path):
        self.path = path
        try:
            with open(path) as f:
                self.db = json.load(f)
        except IOError:
            self.db = {'catalogs': {}}

    @property
    def catalogs(self):
        return self.db['catalogs']

    def sane(self):
        if not self.db:
            return
        if 'catalogs' not in self.db:
            raise ValueError('bad db')
        if type(self.db['catalogs']) is not dict:
            raise ValueError('bad db')

    def get_catalog(self, cat):
        if cat not in self.catalogs:
            self.catalogs[cat] = {}
        return self.catalogs[cat]

    def add_to_catalog(self, cat, gallery, path):
        if cat not in self.catalogs:
            self.catalogs[cat] = {}
        if gallery not in self.catalogs[cat]:
            self.catalogs[cat][gallery] = []
        self.catalogs[cat][gallery].append(path)

        self.commit()

    def commit(self):
        self.sane()
        with open(self.path, 'w') as f:
            json.dump(self.db, f)


def load_from_json(path):
    return SimpleDb(path)

def test_db():
    db = load_from_json('db.json')
    gallery = db.get_catalog('galleries')
    gallery['summer'] = ['a.jpg', 'u.jpg']
    db.commit()

    with open('db.json') as f:
        expected = json.load(f)

    assert expected == {
        'catalogs': { ###### schema 1 per db
            'galleries': {  #### table
                'summer':  ### key, pk
                    [ 'a.jpg', 'u.jpg' ], #arbitrary value
            }
        }
    }

# test_db()